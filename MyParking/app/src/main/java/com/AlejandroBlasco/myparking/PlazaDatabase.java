package com.AlejandroBlasco.myparking;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PlazaDatabase extends SQLiteOpenHelper {
    public PlazaDatabase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists PlazaDatabase(Matricula varchar PRIMARY KEY, Modelo varchar)");
        db.execSQL("insert into PlazaDatabase values('Honda NSX', '4892 DF')");
        db.execSQL("insert into PlazaDatabase values('Ford Mustang', '1489 GHF')");
        db.execSQL("insert into PlazaDatabase values('Mitsubishi Lancer Evolution VI', '5466 XA')");
        db.execSQL("insert into PlazaDatabase values('Audi TT', '6532 BRT')");
        db.execSQL("insert into PlazaDatabase values('Mazda RX-7', '2507 RX')");
        db.execSQL("insert into PlazaDatabase values('Toyota GT-86', '3986 HRK')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
