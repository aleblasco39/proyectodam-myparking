package com.AlejandroBlasco.myparking;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ManejoPlazas extends AppCompatActivity {
    private EditText matricula_et, modelo_et;
    PlazaDatabase plazaDatabase;
    SQLiteDatabase db;
    ListView listaCoches;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manejo_plazas);
        getSupportActionBar().hide();

       matricula_et = findViewById(R.id.matriculaIntro);
       modelo_et = findViewById(R.id.modeloIntro);
       plazaDatabase = new PlazaDatabase(this, "plazas", null, 1);
    }

    public void refrescar(View v){
        listaCoches = findViewById(R.id.ListaCochesAparcados);
        ArrayAdapter<String> adapter;
        List<String> list = new ArrayList<String>();
        Cursor cursor;
        db = plazaDatabase.getReadableDatabase();
        cursor = db.rawQuery("select * from PlazaDatabase", null);
        int num_plaza = 0;
        String datos_coche;
        if (cursor.getCount()==0) list.add("No hay objetos");
        else{
        while (cursor.moveToNext()){
            num_plaza++;
            datos_coche = "Plaza " + num_plaza + ":" + "\n" + cursor.getString(1)
            + "\n" + cursor.getString(0);
            list.add(datos_coche);
        }
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        listaCoches.setAdapter(adapter);
        plazaDatabase.close();
        }
    }

    public void guardarDatos(View v){
        db = plazaDatabase.getWritableDatabase();
        String matricula_coche = matricula_et.getText().toString();
        String modelo_coche = modelo_et.getText().toString();

        if ((matricula_coche.isEmpty()) || (modelo_coche.isEmpty())){
            String mensaje_toast = "No se puede guardar o bien con un campo vacío o bien los dos campos";
            Toast.makeText(this, mensaje_toast, Toast.LENGTH_SHORT).show();
        } else {
            String sql = String.format("Insert into PlazaDatabase values('%s', '%s')", matricula_coche, modelo_coche);
            db.execSQL(sql);
            plazaDatabase.close();
        }
    }

    public void borrarDatos(View v){
        Date d = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String archivo = "Coches día " + df.format(d);
        String dia = df.format(d);

        List<String> list = new ArrayList<>();
        Cursor cursor;
        db = plazaDatabase.getReadableDatabase();
        cursor = db.rawQuery("select * from PlazaDatabase", null);
        int num_plaza = 0;
        String datos_coche;
        String cabecera="Creado el día: "+ dia + "\n" + "Plaza" + "\t"+
                "Modelo coche" + "\t" + "Matricula: ";

        FileOutputStream outputStream;
        try{
            outputStream = openFileOutput(archivo, Context.MODE_PRIVATE);
            outputStream.write(cabecera.getBytes());
            outputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        if (cursor.getCount()==0) list.add("No hay objetos");
        else{
            while (cursor.moveToNext()){
                num_plaza++;
                datos_coche = "Plaza " + num_plaza + ":" + "\t" + cursor.getString(1)
                        + "\t" + cursor.getString(0) + "\n";

                try{
                    outputStream = openFileOutput(archivo, Context.MODE_PRIVATE);
                    outputStream.write(datos_coche.getBytes());
                    outputStream.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        db.execSQL("delete from PlazaDatabase");
        plazaDatabase.close();
    }
}
